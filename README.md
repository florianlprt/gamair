# gamAir

Modeling pollution with Gama.

## applications

- marrakAir (original application, from IRD)
- calAir
- dunkAir

## refs

- [Gama Platform](https://gama-platform.github.io/)
- [Tutorials](https://gama-platform.github.io/wiki/Tutorials) (especially road traffic tutorial)
- [Docs](https://gama-platform.github.io/wiki/Home)
