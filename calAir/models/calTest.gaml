/**
* Name: calTest
* Author: flo
* Description:
* Tags: Tag1, Tag2, TagN
*/

model calTest

global {
	file shape_roads <- shape_file('../includes/shapes/theatre/roads.shp');
	file shape_water <- shape_file('../includes/shapes/theatre/water.shp');
	file shape_buildings <- shape_file('../includes/shapes/theatre/buildings.shp');

	geometry shape <- envelope(shape_roads);

	graph graph_roads;

	int nb_people <- 1000;

	init {
		create road from: shape_roads;
		create water from: shape_water;
		create building from: shape_buildings;
		create people number: nb_people;
		graph_roads <- as_edge_graph(road);
	}
}

species building schedules: [] {
	aspect base {
		draw shape color: #grey depth: 10 #m;
	}
}

species road schedules: [] {
	aspect base {
		draw shape color: #black;
	}
}

species water schedules: [] {
	aspect base {
		draw shape color: #blue;
	}
}

species people skills:[driving]{
	point target <- nil;

	reflex move when: target != location {
		do goto target: target on:graph_roads;
	}

	reflex die when: target = location {
		do die;
	}

	init {
		speed <- 50 #km / #h;
		location <- any_location_in(one_of(road));
		target <- any_location_in(one_of(road));
	}

	aspect base {
		draw circle(10) color: #green;
	}
}


experiment calTest type: gui {
	output {
		display scene type: opengl {
			species road aspect: base;
			species people aspect: base;
			species water aspect: base;
			species building aspect: base;
		}
	}
}
